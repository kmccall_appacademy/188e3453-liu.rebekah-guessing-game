# I/O Exercises
#
# * Write a `guessing_game` method. The computer should choose a number between
#   1 and 100. Prompt the user to `guess a number`. Each time through a play loop,
#   get a guess from the user. Print the number guessed and whether it was `too
#   high` or `too low`. Track the number of guesses the player takes. When the
#   player guesses the number, print out what the number was and how many guesses
#   the player needed.
# * Write a program that prompts the user for a file name, reads that file,
#   shuffles the lines, and saves it to the file "{input_name}-shuffled.txt". You
#   could create a random number using the Random class, or you could use the
#   `shuffle` method in array.
def guessing_game

  correct_num = (1..100).to_a.shuffle.first
  guesses = 0

  while 0
    puts "Guess a number between 1 and 100"
    guess = gets.chomp.to_i
    if guess < correct_num
      puts "#{guess} was too low"
      guesses += 1
    elsif guess > correct_num
      puts "#{guess} was too high"
      guesses += 1
    elsif guess == correct_num
      puts "You won in #{guesses} guesses!"
      break
    end
  end

end
